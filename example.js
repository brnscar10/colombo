 var estados = [
    {
      "name": "SP",
      "cidades": [
        {
          "cidade": "SÃO PAULO",
          "lojas": [
            {
              "local": "JK IGUATEMI",
              "endereco": "Av. Presidente Juscelino Kubitscheck, 2041 - Piso 1 - Itaim Bibi",
              "telefone": "Telefone: (11) 99117-5012 / (11) 3777-7466",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h às 20h "

            },
            {
              "local": "Pátio Paulista",
              "endereco": "Rua Treze de Maio, - Piso Treze de Maio - Bela Vista  São Paulo - SP",
              "telefone": "(11) 98917-1933",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados e fer: 14h ÀS 20h"

            },
            {
              "local": "Shopping Anália Franco",
              "endereco": "Av. Regente Feijó - Piso Lírio - Tatuapé, São Paulo - SP ",
              "telefone": " (11) 2676-2230 / (11) 3777-2743",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h ÀS 20h "

            },
            {
              "local": "Market place",
              "endereco": "Av. Doutor Chucri Zaidan - Piso Térreo - Vila Cordeiro, São Paulo - SP ",
              "telefone": "(11) 3777-3104 / (11) 97969-5087",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"

            },
            {
              "local": "Iguatemi São Paulo",
              "endereco": "Avenida Brigadeiro Faria Lima, 2232 - Jardim Paulistano, - Iguatemi,  São Paulo - SP ",
              "telefone": "(11) 98919-8152 ",
              "atendimento1": "Segunda a Sábado: 10h às 20h",
              "atendimento2": " Domingo e feriados: 12h ÀS 18h"

            },
            {
              "local": "Pátio Higienópolis",
              "endereco": "Av. Higienópolis, - Piso Higienópolis - Higienópolis, São Paulo - SP ",
              "telefone": " (11) 3777-6541 / (11) 96917-5962 ",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h ÀS 20h"

            },
            {
              "local": "Outlet Premium",
              "endereco": "Rodovia dos Bandeirantes, KM 72 - Itupeva, São Paulo - SP ",
              "telefone": "(11) 97692-7617",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h ÀS 20h"

            }
          ]
        },
        {
          "cidade": "SÃO JOSÉ DO RIO PRETO",
          "lojas": [
            {
              "local": "Iguatemi Ribeirão Preto",
              "endereco": "Av. Juscelino Kubitscheck de Oliveira, 5000 - Piso Superior - Iguatemi, São José do Rio Preto - SP",
              "telefone": "(17) 3234-9713 / (17) 99272-1916",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h ÀS 20h"

            }
          ]
        },

        {
          "cidade": "RIBEIRÃO PRETO",
          "lojas": [
            {
              "local": "Iguatemi Ribeirão Preto",
              "endereco": "Av. Luiz Eduardo de Toledo Prato - Piso Superior - Vila do Golf, Ribeirão Preto - SP",
              "telefone": "(11) 3777-4123 / (16) 99159-4146",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": "Domingo e feriados: 14h às 20h"
            }
          ]
        },
        {
          "cidade": "CAMPINAS",
          "lojas": [
            {
              "local": "Iguatemi Campinas",
              "endereco": "Av. Iguatemi, 777 - Piso 2 - Vila Brandina, Campinas - SP ",
              "telefone": "(19) 99334-2553 / (11) 3777-3417",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            }
          ]
        },
        {
          "cidade": "BARUERI",
          "lojas": [
            {
              "local": "Iguatemi Alphavile",
              "endereco": "Alameda Rio Negro III - Piso Xingu - Alphaville Industrial, Barueri - SP ",
              "telefone": " (11) 98847-9800",
              "atendimento1": "Segunda a Sábado: 10h às 22h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            }
          ]
        }
      ]
    },
    {
      "name": "DF",
      "cidades": [
        {
          "cidade": "BRASÍLIA",
          "lojas": [
            {
              "local": "Park Shopping Brasilia",
              "endereco": "SAI/SO Área 6580, s/n - Piso 1 Guará - DF",
              "telefone": "(11) 3777-6532 / (61) 99311-6319 ",
              "atendimento1": "Segunda a Sábado: 11h às 23h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            },
            {
              "local": "Iguatemi Brasília",
              "endereco": "St. de Habitações Individuais Norte CA 4 94, 95 - Lago Norte, Brasília – DF ",
              "telefone": "(11) 99311-3471/ (11) 3468-3873",
              "atendimento1": "Segunda a Sábado: 10h às 14h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            }
          ]
        }
      ]
    },
    {
      "name": "PR",
      "cidades": [
        {
          "cidade": "CURITIBA",
          "lojas": [
            {
              "local": "Park Shopping Barigui",
              "endereco": "Rua Professor Pedro Viriato Parigot de Souza, 600 - Piso Térreo - Ecoville, Curitiba - PR  T",
              "telefone": "(11) 3777-8019 ",
              "atendimento1": "Segunda a Sábado: 11h às 23h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            },
            {
              "local": "Pátio Batel",
              "endereco": "Av. do Batel, 1868 - Piso 2 - Batel, Curitiba - PR",
              "telefone": "(36) 3020-3609 / (11) 377-3416",
              "atendimento1": "Segunda a Sábado: 11h às 23h",
              "atendimento2": " Domingo e feriados: 14h ÀS 20h"
            }
          ]
        }
      ]
    },
    {
      "name": "MG",
      "cidades": [
        {
          "cidade": "BELO HORIZONTE",
          "lojas": [
            {
              "local": "Diamond Mall",
              "endereco": "Av Olegário Maciel 1600, Piso L3, Santo Agostinho - MG ",
              "telefone": "(31) 98391-0458",
              "atendimento1": "Segunda a Sábado: 9h às 22h",
              "atendimento2": "Domingo e feriados: 12h ÀS 21h"
            },
            {
              "local": "BH Shopping",
              "endereco": "BR. 356, n° 3049 - Piso Mariana - Belverde Belo Horizonte - MG",
              "telefone": " (31) 3228-4113 ",
              "atendimento1": "Segunda a Sábado: 9h às 22h",
              "atendimento2": "Domingo e feriados: 12h ÀS 21h"

            }
          ]
        }
      ]
    },
    {
      "name": "CE",
      "cidades": [
        {
          "cidade": "FORTALEZA",
          "lojas": [
            {
              "local": "Iguatemi Fortaleza ",
              "endereco": "Av. Washington Soares, - Piso 2 - Edson Queiroz, Fortaleza - CE ",
              "telefone": " (85) 99281-0448 / (11) 3777-4126 ",
              "atendimento1": "Segunda a Sábado: 9h às 22h",
              "atendimento2": "Domingo e feriados: 12h ÀS 21h"

            }
          ]
        }
      ]
    },
    {
      "name": "RJ",
      "cidades": [
        {
          "cidade": "RIO DE JANEIRO",
          "lojas": [
            {
              "local": "Rio Design Barra",
              "endereco": "Avenida das Américas, 7777 - 2º Piso - Barra da Tijuca Rio de Janeiro - RJ ",
              "telefone": "(21) 3329-0023 / (21) 99363-6825 ",
              "atendimento1": "Segunda a Sábado: 9h às 22h",
              "atendimento2": "Domingo e feriados: 12h ÀS 21h"

            }
          ]
        }
      ]
    }
  ]

  var dropdown = $('.state-box');
  var cityDropdown = $('.city-drop');
  var divStore = $('.store-div');


  $.each(estados, function (key, estado) {
    dropdown.append($('<option></option>').attr('value', estado.name).text(estado.name));
  })

  $(".state-box").on("change", function () {
    var stateSelected = $('.state-box option:selected').val();

    cityDropdown.empty();
    $.each(estados, function (key, estado) {
      if (stateSelected == estado.name) {
        $.each(estado.cidades, function (key, entry) {
          cityDropdown.append($('<option></option>').attr('value', entry.cidade).text(entry.cidade));
          $(".city-drop").on("change", function () {
            var citySelected = $('.city-drop option:selected').val();
            if (citySelected == entry.cidade) {
          
              $('section.store-div').empty();
              divStore.append($('<h2 class="store-color ml0-ns ml5 normal fw5"></h2>').text(entry.cidade))
              $.each(entry.lojas, function (key, loja) {
                var repeat = $($('#template').html());
                repeat.find('.mt3.store-color').text(loja.local);
                repeat.find('.silver:eq(0)').text(loja.endereco);
                repeat.find('.silver:eq(1)').text(loja.telefone);
                repeat.find('.silver:eq(2)').text(loja.atendimento1);
                repeat.find('.silver:eq(3)').text(loja.atendimento2);

                $('section.store-div').append(repeat);
                $('.w-33-ns:eq(7)').parents('.conteudo').addClass('w-100 justify-center flex-ns');
              });
            }
          })
        })
      }
    })
    $('.city-drop').niceSelect('update');
  });
